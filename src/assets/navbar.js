const toggleNavbar = ulist => {
  let links = []

  for (let link of ulist.childNodes) {
    if (link.nodeName !== '#text') {
      links.push(link.childNodes[0])
    }
  }

  links.shift()

  for (let link of links) {
    if (link.style.display !== 'block') {
      link.style.display = 'block'
    } else {
      link.style.display = 'none'
    }
  }
}

const header = document.querySelector('#navbar-header')
const headerImg = document.querySelector('#navbar-header img')

/* The 8 isn't a magic number; it's a combination of the border on
        the selected box and the margin around the image; each is 4px */
headerImg.style.height = (header.offsetHeight - 8).toString() + 'px'
headerImg.style.width = (header.offsetHeight - 8).toString() + 'px'

window.addEventListener('scroll', () => {
  const navbarHeader = document.querySelector('#navbar-header')
  const navbar = document.querySelector('#navbar')
  const offset = window.scrollY

  if (offset >= navbarHeader.offsetHeight) {
    navbar.style.top = (-navbarHeader.offsetHeight).toString() + 'px'
  } else {
    navbar.style.top = (-offset).toString() + 'px'
  }
})
