# debate-site
[![Netlify Status](https://api.netlify.com/api/v1/badges/3a87efad-2366-4eb6-8725-d9a8a3cfc888/deploy-status)](https://app.netlify.com/sites/nmhs-debate/deploys)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
